# Joc
BlackJack o 21

## Funcionamient del joc
Per jugar a Blackjack cal una baralla francesa de 52 cartes, i poden participar d'un a diversos jugadors. Cada carta té un valor numèric assignat de la manera següent:

Les cartes del 2 al 10 valen el seu número.
Les cartes J, Q i K valen 10 punts cadascuna.
L'As pot valer 1 o 11 punts, depenent del que més convingui al jugador en aquell moment.
El joc comença repartint dues cartes cap per amunt a cada jugador, incloent el crupier o la casa. Els jugadors poden veure les seves cartes, però només una de les dues cartes del crupier.

A partir d'aquí, cada jugador té l'opció de demanar més cartes, una per una, amb l'objectiu d'acostar-se el més possible a 21 sense passar-se. Si un jugador suma més de 21 punts, perd automàticament.

Quan tots els jugadors han decidit plantar-se (no demanar més cartes) o s'han passat de 21, el crupier revela la seva segona carta i demana més cartes si la seva puntuació és de 16 o menys, i es planta si en té una puntuació de 17 o més.

El jugador que tingui una puntuació més propera a 21 sense passar-se guanya la mà. Si la puntuació del jugador i del crupier és la mateixa, la partida es considera un empat.

En resum, l'objectiu del Blackjack és obtenir una puntuació el més propera possible a 21 sense superar-la, i guanyar-lo al crupier o als altres jugadors a taula.

## Com jugar

Quan toqui el teu torn, se't repartiran unes cartes aleatòriament. La suma dels seus valors serà la teva puntuació actual. 
Llavors se't preguntarà si vols una carta, hi hauràs d'escriure a la consola "s" o "S" si vols una carta més,
i "n" o "N" si et vols plantar. Si esculls agafar una carta més i la teva puntuació passa de 21, perds.
Si esculls no agafar carta i l'oponent passa de 21, guanyes. Si no esculls agafar una carta més però l'oponent no passa de 21,
el resultat dependrà de les següents cartes que et toquin. Si passen de 21, perds. Si no, tornem a repetir el procès. 



## Autor
juditperea
